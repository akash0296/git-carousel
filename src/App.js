import React, { Component } from 'react';
import CustomComponent from './components/CustomComponent';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <CustomComponent />
      </div>
    );
  }
}

export default App;
