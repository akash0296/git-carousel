import React, { Component } from 'react'
import './style.scss';
import axios from 'axios';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

class CustomComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    }
  }

  componentDidMount() {
    this.apirequest(); 
    console.log(this.state.data); 
  }

  apirequest = async () => {
    const response = await axios.get('https://api.github.com/users');
    this.setState({data : response.data});
    console.log(this.state.data);
  }

  render() {
    return (
      <div>
        <h1>Git Carousel</h1>
        <ul style={{width: '60%', margin: '0 auto', height: 'auto'}}>
          <Carousel infiniteLoop={true} autoPlay={true} showThumbs={false}>
          {
            this.state.data ? this.state.data.map((item,index) => {
              return (
                <li key={item.id}>
                  <div>
                    <img src={item.avatar_url} />
                    <h2 className="legend">{item.login} - id: {item.id}</h2>
                  </div>
                </li>
              )
            }) : <p>Loading....</p>
          }
          </Carousel>
        </ul>
      </div>
    )
  }
}

export default CustomComponent;